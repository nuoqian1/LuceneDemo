package com.bxoon.dao;

import com.bxoon.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ZGX99 on 2018/1/22.
 */
public interface UserDao extends JpaRepository<User,Integer> {
    User findByUserNameAndPassword(String userName, String password);

    User findByUserName(String userName);
}
