package com.bxoon.service;

import com.bxoon.dao.UserDao;
import com.bxoon.domain.DataModel;
import com.bxoon.domain.User;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据管理Service
 */
@Service
public class DataManageService {

    @Autowired
    UserDao userDao;

    /**
     * 数据采集
     */
    private List<DataModel> getData(){
        List<User> list = userDao.findAll();
        List<DataModel> dataModelList = new ArrayList<>(list.size());
        for (User user:list){
            DataModel dataModel = new DataModel();
            dataModel.setId(user.getId()+"");
            dataModel.setName(user.getName());
            dataModelList.add(dataModel);
        }
        return dataModelList;
    }

    /**
     * 创建索引
     */
    public void createIndex(){
        try {
            List<DataModel> dataModelList = getData();
            List<Document> documentList = new ArrayList<>();
            dataModelList.forEach(e -> {
                Document document = new Document();
                Field idField = new TextField("id", e.getId(), Field.Store.YES);
                Field nameFiled = new TextField("name", e.getName(), Field.Store.YES);
                document.add(idField);
                document.add(nameFiled);
                documentList.add(document);
            });

            IKAnalyzer ikAnalyzer = new IKAnalyzer();
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(ikAnalyzer);
            File indexFile = new File("E:\\LuceneRepo\\");
            Path path = indexFile.toPath();
            Directory directory = FSDirectory.open(path);
            IndexWriter writer = new IndexWriter(directory, indexWriterConfig);
            writer.addDocuments(documentList);
            writer.commit();
            writer.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void indexSearch(){
        try {
            // 创建query对象
            // 使用QueryParser搜索时，需要指定分词器，搜索时的分词器要和索引时的分词器一致
            // 第一个参数：默认搜索的域的名称
//            QueryParser parser = new QueryParser("name", new IKAnalyzer());
//            parser.setAllowLeadingWildcard(true);
//            Query query = parser.parse("name:张");
            Term aTerm = new Term("name", "张三");
            Query query = new TermQuery(aTerm);

            // 创建IndexSearcher
            // 指定索引库的地址
            File indexFile = new File("E:\\LuceneRepo");
            Path path = indexFile.toPath();
            Directory directory = FSDirectory.open(path);
            DirectoryReader ireader = DirectoryReader.open(directory);
            IndexSearcher searcher = new IndexSearcher(ireader);

            // 通过searcher来搜索索引库
            // 第二个参数：指定需要显示的顶部记录的N条
            TopDocs topDocs = searcher.search(query, 100);
            ScoreDoc[] scoreDocs = topDocs.scoreDocs;
            for (ScoreDoc scoreDoc:scoreDocs){
                int scoreDocId = scoreDoc.doc;
                Document document = searcher.doc(scoreDocId);
                System.out.println("id:"+document.get("id"));
                System.out.println("name:"+document.get("name"));
            }
            directory.close();
            ireader.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
