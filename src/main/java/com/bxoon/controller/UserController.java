package com.bxoon.controller;

import com.bxoon.dao.UserDao;
import com.bxoon.domain.User;
import com.bxoon.service.DataManageService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    public UserDao userDao;

    @Autowired
    DataManageService dataManageService;

    /**
     * helloworld
     *
     * @return
     */
    @ApiOperation(value = "测试HelloWorld", notes = "HelloWorld")
    @ApiImplicitParam(name = "user", value = "测试HelloWorld", required = true, dataType = "")
    @RequestMapping(value = "HelloWorld", method = RequestMethod.GET)
    public String helloworld() {
        System.out.println(userDao.findAll().size());
        return "helloworld";
    }

    /**
     * 测试Lucene
     * @return
     */
    @RequestMapping(value = "testLucene")
    public String testLucene(){
        dataManageService.createIndex();
        return "success";
    }

    /**
     * 测试Lucene查询
     * @return
     */
    @RequestMapping(value = "testLuceneSearch")
    public String testLuceneSearch(){
        dataManageService.indexSearch();
        return "success";
    }

}
